<!DOCTYPE html>
<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title></title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		<meta http-equiv="refresh" content="10" />
	</head>
	<body><div class="container-fluid"><div class="row">
		<img src="DinoTrackMap.png" style="width: 100%;height: 100%">
		<a href="#" data-toggle="modal" data-target="#scene19"><div style="position: absolute; left: 14%; top: 22.4%;width:15px; height: 15px; border-radius:100%; background-color: #008000;"></div></a>
		<a href="#" data-toggle="modal" data-target="#scene18"><div style="position: absolute; left: 9.2%; top: 20.7%;width:15px; height: 15px; border-radius:100%; background-color: #FF0033;"></div></a>
		</div></div>
		<!-- Modal -->
		<div class="modal fade" id="scene18" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Scene 18 Status</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<table class="table table-bordered table-hover">
							<thead class="thead-inverse">
								<tr>
									<th>Alarm Number</th>
									<th>Severity</th>
									<th>Time</th>
									<th>Discipline</th>
									<th>Alarm Description</th>
									<th>Link to Troubleshooting</th>
								</tr>
							</thead>
							<tr>
								<td>SL.SC18.RIO.01</td>
								<td>Warning</td>
								<td>7/1/2017 3:14:01 PM</td>
								<td>Show Lighting</td>
								<td>Scene 18 Remote DMX Unit Not Responding</td>
								<td><a href="#">Remote Units</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="scene19" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Scene 19 Status</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>No Issues Reported</p>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-inverse bg-inverse fixed-bottom navbar-toggleable-sm">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="alarms.php">Daily Alarm List</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="active.php">Active Alarms</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="history.php">Full Alarm List</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="trackmap.php">Attraction Map</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Attraction Status</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Troubleshooting Guide</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="config.php">Attraction Configuration</a>
				</li>
			</ul>
		</nav>
	</body>
</html>