<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/css/chosen.min.css">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		<script src="js/chosen.jquery.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-2" style="padding-left: 0px;">
					<!-- Nav tabs -->
					<ul class="nav flex-column bg-inverse nav-pills" role="tablist">
						<li class="nav-item">
							<a class="nav-link active text-white" data-toggle="tab" href="#client" role="tab">Client</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" data-toggle="tab" href="#system" role="tab">System</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" data-toggle="tab" href="#alarm" role="tab">Alarm Config</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" data-toggle="tab" href="#zone" role="tab">Zone Config</a>
						</li>
					</ul>
				</div>
				<div class="col">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="client" role="tabpanel">
							<h2>Client Configuration</h2>
							<br>
							<div class="card">
								<div class="card-header">
									Display Config
								</div>
								<div class="card-block">
									<form>
										<div class="form-group">
											<legend for="colorSelector">Color Format</legend>
											<select class="form-control" id="colorSelector">
												<option>Universal HMI Spec</option>
												<option>Disney</option>
												<option>Custom File</option>
												<option>EOS Default</option>
												<option>Selene</option>
											</select>
										</div>
										<div class="form-group">
											<legend>TrackMap Display</legend>
											<div class="form-check">
												<label class="form-check-label">
													<input type="radio" class="form-check-input" name="trackmapenable" id="trackmapenable1" value="true">
													Enable
												</label>
											</div>
											<div class="form-check">
												<label class="form-check-label">
													<input type="radio" class="form-check-input" name="trackmapenable" id="trackmapenable2" value="false">
													Disable
												</label>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="system" role="tabpanel">
							System
						</div>
						<div class="tab-pane" id="alarm" role="tabpanel">
							Alarm Config
						</div>
						<div class="tab-pane" id="zone" role="tabpanel">
							Zone Config
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-inverse bg-inverse fixed-bottom navbar-toggleable-sm">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="alarms.php">Daily Alarm List</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="active.php">Active Alarms</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="history.php">Full Alarm List</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="trackmap.php">Attraction Map</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Attraction Status</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Troubleshooting Guide</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="config.php">Attraction Configuration</a>
				</li>
			</ul>
		</nav>
		<script>
			$(document).ready(function() {
				$(".chosen-select").chosen()
			} );
		</script>
	</body>
</html>