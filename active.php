<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	</head>
	<body>
		<table class="table table-bordered table-hover">
			<thead class="thead-inverse">
				<tr>
					<th>Alarm Number</th>
					<th>Severity</th>
					<th>Time</th>
					<th>Discipline</th>
					<th>Alarm Description</th>
					<th>Location</th>
					<th>Link to Troubleshooting</th>
				</tr>
			</thead>
			<?php 

				$host = 'localhost';
				$db =	'EOS';
				$user = 'root';
				$pass = 'Frozen724816';
				$charset = 'utf8';

				$dsn = "mysql:host=$host;dbname=$db;charset=$charset";


				$pdo = new PDO($dsn, $user, $pass);
				foreach ($pdo ->query('SELECT * FROM alarms WHERE isActive=TRUE ORDER BY id DESC') as $row) {
					$output = '<tr class="';

					switch ($row['severityId']) {
						case '0':
							$output = $output.'table-danger';
							break;

						case '1':
							$output = $output.'table-warning';
							break;

						case '2':
							$output = $output.'table-info';
							break;
						
						case '5':
							break;
					}

					$output = $output.'"><td>'.$row['alarmCode'].'</td><td>';
					$output = $output.$row['severity'].'</td><td>'.$row['timeDate'];
					$output = $output.'</td><td>'.$row['discipline'].'</td><td>'.$row['description'];
					$output = $output.'</td><td><a href="filteralarms.php?id='.$row['locationId'].'">'.$row['locationText'].'</a></td><td><a href="helpfiles.php?id='.$row['helpFileNo'].'">'.$row['helpFileText'].'</a></td></tr>';
					echo $output;
				}

			?>
		</table>
		<nav class="navbar navbar-inverse bg-inverse fixed-bottom navbar-toggleable-sm">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="alarms.php">Daily Alarm List</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="active.php">Active Alarms</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="history.php">Full Alarm List</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="trackmap.php">Attraction Map</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Attraction Status</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Troubleshooting Guide</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="config.php">Attraction Configuration</a>
				</li>
			</ul>
		</nav>
	</body>
</html>